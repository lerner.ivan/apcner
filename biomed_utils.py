import matplotlib.pyplot as plt
import numpy as np
from main import easyTrain, easyDetailedDecoding
from hyperopt import  fmin, tpe, hp, STATUS_OK, Trials
import os
import pickle
import pandas as pd
import re
import time

from sklearn.metrics import f1_score, recall_score, precision_score, confusion_matrix
import itertools


def compute_corpus_stats_on_conll(path):
    #read
    with open(path, 'r') as h:
        preds = []
        sent=[]
        sent_count= 0
        for line in h.readlines():

            if line=='\n':
                preds.append(sent)
                sent_count+=1
                sent=[]
            else:
                sent.append(re.split(" |\n", line)[:-1]+[sent_count])

    #to pandas
    preds = pd.DataFrame([t for sub in preds for t in sub], columns=['tok', 'lab', "sent_count"])


    results_counts = {}

    results_counts['n_sent'] = preds.sent_count.nunique()

    results_counts['n_tok'] = preds.tok.count()
    results_counts['median_tok_per_sent'] = preds.groupby('sent_count')['tok'].count().median()
    results_counts['n_voc'] = preds.tok.nunique()

    #compute chunks
    #true chunk
    chunk_ids = []
    chunk_type = [0]
    chunk_id = 0


    for i, row in preds.iterrows():
        #true labels chunk
        if row.lab == 'O':
            if chunk_type[-1] != 0:
                chunk_id +=1
            chunk_ids.append(chunk_id)
            chunk_type.append(0)
        elif row.lab[0]=="B":
            chunk_id +=1
            chunk_ids.append(chunk_id)
            chunk_type.append(1)

        elif row.lab[0] == "I":
            chunk_ids.append(chunk_id)
            chunk_type.append(1)



    preds["chunk_id1"] = chunk_ids
    preds["chunk_type1"] = chunk_type[1:]



    #compute span taking account of \n and " "
    preds = (preds
             .assign(toklen =  lambda x:(x.tok.str.len()))
             .assign(endspan = lambda x:(x.toklen  + 1).cumsum()-1)
             .assign(startspan = lambda x:x.endspan-x.toklen)
            )

    #to brat format
    brat_true = (preds.loc[lambda x:x.chunk_type1==1]
            .groupby('chunk_id1')
            .apply(lambda x: x.lab.tolist()[0].split('-')[-1] +" "+str(x.startspan.values[0]) +" "+str(x.endspan.values[-1])+"\t"+ " ".join(x.tok.tolist()))
           )




    brat_true = (preds.loc[lambda x:x.chunk_type1==1]
            .groupby('chunk_id1')
            .apply(lambda x: x.lab.tolist()[0].split('-')[-1]+"\t"+ " ".join(x.tok.tolist()) + "\t" + str(len(x.tok.tolist())))
           )

    brat_true = brat_true.str.split('\t', expand=True)

    brat_true.columns = ['ent', 'txt', "n_tok"]

    brat_true.n_tok = brat_true.n_tok.astype('int')

    results_counts['n_ent'] = brat_true.ent.count()

    results_counts['n_ent_types'] = brat_true.groupby('ent')['txt'].count().to_dict()

    results_counts['n_unigram'] = brat_true.loc[brat_true.n_tok==1].groupby('ent')['txt'].count().to_dict()

    results_counts['n_bigram'] = brat_true.loc[brat_true.n_tok==2].groupby('ent')['txt'].count().to_dict()

    results_counts['n_trigramplus'] = brat_true.loc[brat_true.n_tok>2].groupby('ent')['txt'].count().to_dict()
    
    return results_counts

def conll_to_brat(path):
    """From a connl eval file with token pred-label true-label
   --> to BRAT format 
    """
    
    #read
    with open(path, 'r') as h:
        preds = []
        sent=[]
        sent_count= 0
        for line in h.readlines():

            if line=='\n':
                preds.append(sent)
                sent_count+=1
                sent=[]
            else:
                sent.append(re.split(" |\n", line)[:-1]+[sent_count])

                
    #to pandas
    preds = pd.DataFrame([t for sub in preds for t in sub], columns=['tok', 'pred', 'lab', "sent_count"])

    

    #compute chunks
    #true chunk
    chunk_ids = []
    chunk_type = [0]
    chunk_id = 0

    #pred chunk
    chunk_ids2 = []
    chunk_type2 = [0]
    chunk_id2 = 0
    for i, row in preds.iterrows():
        #true labels chunk
        if row.lab == 'O':
            if chunk_type[-1] != 0:
                chunk_id +=1
            chunk_ids.append(chunk_id)
            chunk_type.append(0)
        elif row.lab[0]=="B":
            chunk_id +=1
            chunk_ids.append(chunk_id)
            chunk_type.append(1)

        elif row.lab[0] == "I":
            chunk_ids.append(chunk_id)
            chunk_type.append(1)

        #pred chunk
        #true labels chunk
        if row.pred == 'O':
            if chunk_type2[-1] != 0:
                chunk_id2 +=1
            chunk_ids2.append(chunk_id2)
            chunk_type2.append(0)
        elif row.pred[0]=="B":
            chunk_id2 +=1
            chunk_ids2.append(chunk_id2)
            chunk_type2.append(1)

        elif row.pred[0] == "I":
            chunk_ids2.append(chunk_id2)
            chunk_type2.append(1)

    preds["chunk_id1"] = chunk_ids
    preds["chunk_type1"] = chunk_type[1:]

    preds["chunk_id2"] = chunk_ids2
    preds["chunk_type2"] = chunk_type2[1:]

    #compute span taking account of \n and " "
    preds = (preds
             .assign(toklen =  lambda x:(x.tok.str.len()))
             .assign(endspan = lambda x:(x.toklen  + 1).cumsum()-1)
             .assign(startspan = lambda x:x.endspan-x.toklen)
            )
    
    #to brat format
    brat_true = (preds.loc[lambda x:x.chunk_type1==1]
            .groupby('chunk_id1')
            .apply(lambda x: x.lab.tolist()[0].split('-')[-1] +" "+str(x.startspan.values[0]) +" "+str(x.endspan.values[-1])+"\t"+ " ".join(x.tok.tolist()))
           )

    brat_true = ["T"+str(i)+ "\t" for i in range(len(brat_true))]+ brat_true


    brat_pred = (preds.loc[lambda x:x.chunk_type2==1]
            .groupby('chunk_id2')
            .apply(lambda x: x.pred.tolist()[0].split('-')[-1] +" "+str(x.startspan.values[0]) +" "+str(x.endspan.values[-1])+"\t"+ " ".join(x.tok.tolist()))
           )

    brat_pred = ["T"+str(i)+ "\t" for i in range(len(brat_pred))]+ brat_pred

    #to text
    text = "\n".join(preds.groupby("sent_count")['tok'].apply(lambda x:" ".join(x.tolist())).tolist())

    
    
    #save
    with open("../data/eval/brat_true/brat.ann", "w") as h:
        h.write("\n".join(brat_true.tolist()))

    with open("../data/eval/brat_pred/brat.ann", "w") as h:
        h.write("\n".join(brat_pred.tolist()))

    with open("../data/eval/brat_pred/brat.txt", "w") as h:
        h.write(text)

    with open("../data/eval/brat_true/brat.txt", "w") as h:
        h.write(text)

def non_exact_eval(path):

    #check
    for path2rm in ["../data/eval/brat_true/brat.ann", "../data/eval/brat_true/brat.txt",
                    "../data/eval/brat_pred/brat.ann", "../data/eval/brat_pred/brat.txt"]:
        if os.path.exists(path2rm):
            os.remove(path2rm)

    assert os.path.exists(path)
    conll_to_brat(path)

    results = os.popen("java -cp BRATEval-0.0.2-SNAPSHOT.jar au.com.nicta.csp.brateval.CompareEntities ../data/eval/brat_true/ ../data/eval/brat_pred/ False").read()
    results = re.split("\n", results.split('\n\n')[1])[1:-1]
    results = pd.DataFrame([t.split('\t') for t in results][1:], columns=[t.split('\t') for t in results][0]).set_index("")
    
    if len(results)==2:
        results = results.drop("Overall")
    
    return results



def plot_confusion_matrix(cm, classes,
                          normalize=False,
                          title='Confusion matrix',
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')


    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    plt.tight_layout()

def token_wise_eval(path, name):
    with open(path, 'r') as h:
        preds = h.readlines()

    preds = [t for t in preds if t!='\n']

    assert [t for t in preds if len(t.split())!=3] == []

    preds = [t.split() for t in preds]

    preds = pd.DataFrame(preds, columns=['tok', 'pred', 'true'])

    non_exact_preds = preds.loc[:, ["true", "pred"]].apply(lambda x:x.str.replace('[A-Z]\-', ""), axis=0)    
    classes = non_exact_preds.true.unique()
    
    if non_exact_preds.true.nunique()==2:
        preds = preds.assign(pred = lambda x:x.pred!='O').assign(true = lambda x:x.true!='O')

        f = f1_score(preds.true, preds.pred)
        r = recall_score(preds.true, preds.pred)
        p = precision_score(preds.true, preds.pred)
        cm = confusion_matrix(preds.true, preds.pred)
        
    else:
        print("Multilabel mode")
        f = f1_score(pd.get_dummies(non_exact_preds.true), pd.get_dummies(non_exact_preds.pred), average='samples')
        r = recall_score(pd.get_dummies(non_exact_preds.true), pd.get_dummies(non_exact_preds.pred), average='samples')
        p = precision_score(pd.get_dummies(non_exact_preds.true), pd.get_dummies(non_exact_preds.pred), average='samples')
        cm = confusion_matrix(non_exact_preds.true, non_exact_preds.pred, labels=classes)

    print("Token wise metrics for (non exact match) {:>40} : F-mesure:\t{:.3f} ; Recall:\t{:.3f} ; Precision:\t{:.3f} \n".format(name, f, r, p))
    
    return f, r, p, cm, classes
#training
def conlleval_pandas(detailed_r):
    results_df = (pd.DataFrame([ [t for t in r.split(' ') if t] for r in detailed_r.split('\n')[2:]])
     .dropna()
     .append(pd.DataFrame(['All types']+[ r for r in detailed_r.split('\n')[1].split(' ') if r][2:] +[detailed_r.split('\n')[0].split(' ')[7]]).T)
     .apply(lambda x:x.str.replace(':|;',''))
     .rename(columns={0:'Entity type', 2:'Precision', 4:'Recall', 6:'F-mesure', 7:'# predicted positive'})
     .set_index('Entity type')
     .drop([1,3,5], axis=1)
    )

    results_df = results_df.assign(TP = ((results_df.Precision.str.replace('%','').astype('float')*0.01 \
                             * results_df.loc[:,"# predicted positive"].astype('float'))
                            .round().astype('int')
                           )
                     )
    
    tokens_accuracy = (pd.DataFrame(detailed_r.split('\n')[0].split(' ')[1:6] + detailed_r.split('\n')[1].split(' ')[:3]).T
         .rename(columns={0:'tokens', 3:'phrases',7:'Accuracy'})
         .drop([1,2,4,5,6], axis=1)
         .apply(lambda x:x.str.replace(':|;',''))

        )
    
    
    results_df = (results_df.assign(Precision = lambda x:x.Precision.str[:-1].astype('float'))
                  .assign(Recall = lambda x:x.Recall.str[:-1].astype('float'))
                  .assign(tokens = tokens_accuracy.tokens.item())
                  .assign(phrases = tokens_accuracy.phrases.item())
                  .assign(Accuracy = tokens_accuracy.Accuracy.str[:-1].astype('float').item())
                  )
    
    
    
    return results_df


### Objective function
def reshape_conf(params):
    """    Map the output of an hyperopt space sample to the input of the NER model.
        input : sample of hyperopt space
        output : configuration for NER model
"""
    #redefine params
    if params.get('net_params') is not None:  
        params['net_params'] = {k.split('__')[-1]:v for (k,v) in params['net_params'].items()}
        params = {**params , **params['net_params']}
        del params['net_params']


    #unnest
    if params.get('char_params') is not None:  
        params = {**params , **params['char_params']}
        del params['char_params']
    #non negative not none
    params['use_weights'] = params["class_weights"]['use_weights']
    params['use_crf'] = params["class_weights"]['use_crf']

    del params["class_weights"]['use_weights'], params["class_weights"]['use_crf']
    if params.get('class_weights') is not None:  
        params['class_weights'] = {k : max(v,0.01) for k,v in params['class_weights'].items()}
    params = {**{k:v for k,v in params.items() if v !='None'}, **{k:None for k,v in params.items() if v =='None'}}
    params['HP_dropout'] = max(params['HP_dropout'], 0.1)
    params['HP_dropout'] = min(params['HP_dropout'], 0.9)

    #type int
    if params.get('HP_char_hidden_dim') is not None:    
        params['HP_char_hidden_dim'] = int(params['HP_char_hidden_dim'])
    if params.get('HP_cnn_layer') is not None:    
        params['HP_cnn_layer'] = int(params['HP_cnn_layer'])
    if params.get('HP_lstm_layer') is not None:   
        params['HP_lstm_layer'] = int(params['HP_lstm_layer'])
    if params.get('HP_hidden_dim') is not None:   
        params['HP_hidden_dim'] = int(params['HP_hidden_dim'])
    if params.get('feat_config') is not None:
        params['feat_config'] = {k:{k.split('__')[0]:int(v) if k.split('__')[0]=="emb_size" else v for k,v in v.items()} for k,v in params['feat_config'].items()}
    
    return params

def objective(params):
    """
    Map the output of an hyperopt space sample to the input of the NER model.
    Train the model with the parameters sampled from the hyperopt space.
    
    input : sample of hyperopt space
    output : dictionnary of results
    """
    params = reshape_conf(params)
    #train
    if 'cv_dir' in params.keys():
        assert 'train_dir' not in params
        assert 'dev_dir' not in params
        assert 'test_dir' not in params

        #find folds
        N_fold = params['cv_dir'][1]
        params['cv_dir'] = [re.sub(r"\d", str(i), params['cv_dir'][0]) for i in range(N_fold)]*2
        
        #cv
        dev_scores = []
        train_score = []
        epoch = []
        for i in range(N_fold):
            if params['save_model']:
                params['train_dir'] =  params['cv_dir'][i:i+N_fold-1]
                params['dev_dir']= params['cv_dir'][i+N_fold-1]
            else:
                params['train_dir'] =  params['cv_dir'][i:i+N_fold-1]
                params['dev_dir'] = params['cv_dir'][i+N_fold-1]
            
            part_dev,_, part_train, part_epoch = easyTrain(params)
            
            dev_scores.append(part_dev)
            train_score.append(part_train)
            epoch.append(part_epoch)
        dev_score = np.mean(dev_scores)

            
    else:
        dev_score,_, train_score, epoch = easyTrain(params)
        dev_scores = dev_score
    return {
        
        'loss': -dev_score,
        'status': STATUS_OK,
        'train_score': train_score,
        'dev_score': dev_scores,
        'epoch': epoch
    }

class HyperoptTrials(object):
    def __init__(self, path, space):
        self.path = path
        if os.path.exists(self.path ):
            with open(self.path , 'rb') as h:
                self.trials = pickle.load(h)
                
            self.step = len(self.trials) // 10
        else:
            self.trials = Trials()
            self.step = 1
            
        self.space = space
    def train(self, NMAX, PERIOD_OF_TIME = 60*60*24*7 ):
        start = time.time()

        while True:
            for i in range(self.step, NMAX//10 + 1):
                if os.path.exists(self.path):
                    with open(self.path , 'rb') as h:
                        self.trials = pickle.load(h)
                best = fmin(objective, self.space, algo=tpe.suggest, max_evals=i*10, trials=self.trials)
                with open(self.path , 'wb') as h:
                    pickle.dump(self.trials, h, protocol=pickle.HIGHEST_PROTOCOL)

                if i % 20 == 0:
                    print(i*10, "done")
            if time.time() > start + PERIOD_OF_TIME : break



### plot hyperopt results
def plot_param_search(result, param = None, save_to = None, with_train = False, focus = (-1, 1)):
    """plot hyperopt parameters search results"""
    #only ok trials
    result = [t for t in result if t['result']['status']=='ok']
    #set param
    if param is None:
        param = [t for t in result[0]['misc']['vals']]
    else:
        pass
    n_param = len(param)
    #plot
    num_row = np.ceil(n_param/3).astype('int')
    f, axes = plt.subplots(nrows = num_row, ncols=3, figsize=(15, num_row*2), sharey=True)
    cmap = plt.cm.jet
    
    for i, val in enumerate(param):
        #get results
        xs = np.array([t['misc']['vals'][val] for t in result if t['misc']['vals'][val]])
        y_dev = np.array([t['result']['dev_score'] for t in result if t['misc']['vals'][val]])
        y_train = np.array([t['result']['train_score'] for t in result if t['misc']['vals'][val]])

        if with_train:
            #reformat
#             xs = [(xi[0], xi[0]) for xi in xs]
#             ys = [(t,d) for t,d in zip(y_train, y_dev)]
#             xs_ys = [(xi, yi) for (xi, yi) in zip(xs, ys)]
#             xs_ys = [t for sub in xs_ys for t in sub]


#             # plot
#             axes[i//3,i%3].plot(*xs_ys, alpha=0.4)
#             axes[i//3,i%3].set_title(val)
#             axes[i//3,i%3].set_ylim(-1,1)
            axes[i//3,i%3].scatter(xs, y_train - y_dev, s=20, linewidth=0.01, alpha=0.5, c='red')
            axes[i//3,i%3].set_title(val)
            axes[i//3,i%3].set_ylim(focus[0],focus[1])

        else:
            # plot
            axes[i//3,i%3].scatter(xs, y_dev, s=20, linewidth=0.01, alpha=0.5, c='red')
            axes[i//3,i%3].set_title(val)
            axes[i//3,i%3].set_ylim(focus[0],focus[1])

    plt.subplots_adjust(hspace=0.5, top=0.9)

    #plt.tight_layout()
    if save_to is not None:
        plt.savefig(save_to, format="png")